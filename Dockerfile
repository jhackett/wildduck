FROM nodemailer/wildduck

COPY api.toml /wildduck/config/api.toml
COPY post-entrypoint.sh /post-entrypoint.sh

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD sh /post-entrypoint.sh && node ${WILDDUCK_APPDIR}/server.js --config=${WILDDUCK_CONFIG} ${CMD_ARGS}
